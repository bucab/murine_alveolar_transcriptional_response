# This script will load the DE result from Bewley group for the healthy human and compare with my DE result.

library(VennDiagram)
library(tidyverse)
library(ggpubr)
library(kableExtra)

#load the bewley res for the healthy human DE
bew_sig <- read_csv('../BewleyData/AM_MI_vs_Spn.csv') %>%
  filter(adj.P.Val < 0.05) 

# load my sig data
my_sig <- read_csv('../output/differential_expression_statistics_healthy_samples_Bewley_SIG.csv')

# correlation between the log2 FC
cor.test(bew_sig %>%
           arrange(X1) %>%
           pull(logFC) , 
         my_sig %>%
           filter(probesetID %in% bew_sig$X1 ) %>%
           arrange(probesetID) %>%
           pull(logFC), 
         method = 'spearman')

cor.test(bew_sig %>%
           arrange(X1) %>%
           pull(AveExpr), 
         my_sig %>%
           filter(probesetID %in% bew_sig$X1 ) %>%
           arrange(probesetID) %>%
           pull(AveExpr), 
         method = 'spearman')

# drawing venn diagram
diff_list <- list(bewley = bew_sig$X1,
                  my = my_sig$probesetID)
fill <- c('#00468B', '#EC0000')
size  <- rep(0.5, 2)
venn <- venn.diagram(x = diff_list, 
                     filename = NULL,
                     height = 2000,
                     width = 2000, fill = fill,
                     cat.default.pos = "text", 
                     cat.cex = size,
                     main = "Overlap between Bewley and I sig result");
png('../figs/Overlap between Bewley and I sig result.png', width = 4, height = 4, units = 'in', res = 300)
grid.draw(venn)
dev.off()


knitr::include_graphics('../figs/Overlap between Bewley and I sig result.png')
```


